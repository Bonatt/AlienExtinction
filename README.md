### A single alien lands on Earth...
Every day after that, each alien on Earth undergoes a transformation, which could be any of the four equally likely events:

1. the alien dies,
2. the alien does nothing,
3. the alien replicates itself (2 aliens total),
4. the alien replicates itself twice (3 aliens total).

With bad luck the alien race might die out quickly. But with good luck the alien race might survive indefinitely.
What is the probability the alien race eventually dies out and goes extinct?
[This video presents a solution to this problem](https://www.youtube.com/watch?v=A5-Q2GdD5xw), which has been asked as a technical job interview question.

There are a few ways to go about finding a solution. Here's mine:
An alien lands on Earth (one iteration) and does, with equal probability, one of the above options.
If no aliens exist after the event, a new iteration starts. Otherwise, repeat the following day. 

Now... for every every day at least one alien is alive, this event can be chosen _per alien_ or _per population_, i.e.
for the current population we have two options:
1. for i in aliens: choose event
2. all aliens choose same event

For large enough iterations, I think option 2 works just fine. 
I am not sure if this is mathematically correct, but my code yields a correct result.
I chose the second method because it was computationally faster.

For each iteration I logged: the number of aliens after some max number of days ("Aliens")
(I will get to it soon, but after approximately 50 days the probability of a population ever becoming extinct is approximately zero),
the number of days a population was greater than zero (again, a maximum of 50) ("Days"),
and a running average of the extinction rate ("Extinct %"). These were output below for 100 and 10000 iterations, respectively.

Notice how with relatively few iterations the extinction probablity varies greatly.
If I somehow put proper uncertainties on the Extinct %, the uncertainties would be significant.

```
Iter.   | Days  | Aliens | Extinct %
--------------------------------------------
 0      |  0    |  1
 10     |  50   |  31    |  30.0 %
 20     |  2    |  0     |  40.0 %
 30     |  50   |  19    |  46.6666666667 %
 40     |  50   |  35    |  45.0 %
 50     |  50   |  43    |  42.0 %
 60     |  50   |  31    |  46.6666666667 %
 70     |  50   |  36    |  44.2857142857 %
 80     |  50   |  20    |  42.5 %
 90     |  50   |  30    |  42.2222222222 %
 100    |  50   |  27    |  43.0 %
--------------------------------------------
Iter.   | Days  | Aliens | Extinct %
```

With 10000 iterations (even by 1000) the Extinct % smooths out to some value.
This is a demonstration of the [Law of Large Numbers](https://en.wikipedia.org/wiki/Law_of_large_numbers).
And check out the plot just below.

```
Iter.   | Days  | Aliens | Extinct %
--------------------------------------------
 0      |  0    |  1
 1000   |  50   |  42    |  41.9 %
 2000   |  7    |  0     |  41.05 %
 3000   |  4    |  0     |  40.6 %
 4000   |  50   |  15    |  40.125 %
 5000   |  50   |  40    |  40.32 %
 6000   |  50   |  29    |  40.3333333333 %
 7000   |  50   |  22    |  40.7428571429 %
 8000   |  50   |  26    |  41.0375 %
 9000   |  50   |  18    |  41.1 %
 10000  |  50   |  42    |  41.13 %
--------------------------------------------
Iter.   | Days  | Aliens | Extinct %

 Iterations    10000
 Max days      50
 Survived      58.87 %
 Extinct       41.13 %

Runtime 0.1 minutes
```

![Approach](AlienExtinction_approach_50Days_10000Iters.png)

__The probablity of the alien population becoming extinct converges to approximately 41%.__

But to save computation time, and get more interesting data, how did I discover that 50 days was the approximate point of no return 
(or, the number of a days one should observe the alien population before it can marked as inextinctable)?
I created a few different types of plots (one that I thought was cool but is now in a previous commit) but eventually
settled on the readbility of this representation:

![Optimzie](AlienExtinction_optimize_50Days_10000Iters.png)

Filling a histogram with the day each population became extinct produced the plot above. One can see that the probability a population will become extinct after the first day is 25%, exactly what is expected. And the probability quickly decreases to near zero by the time 50 days have passed.


I could observe a population for more than 50 days (say, 1000), but the tail dissipates well before 100 days.
And I could increase the number of landings (say, 100000) to create a smoother dataset, and thus a prettier plot, but the increased computation time doesn't yield a significantly more-accurate result. You'll just have to believe me on these two points. 
