import ROOT
import numpy as np
import time
start_time = time.time()
#print("--- %s seconds ---" % (time.time() - start_time))


# :%s/foo/bar/gc  Change each 'foo' to 'bar', but ask for confirmation first.

# Redefine quit to something shorter
def ex():
  quit()
# Redefine np.sqrt() to sqrt()...
def sqrt(x):
  return np.sqrt(x)
#///// Set global style /////
ROOT.gROOT.SetStyle('Plain');
ROOT.gStyle.SetPalette(53);
#// Boxes
ROOT.gStyle.SetLegendFont(132);
ROOT.gStyle.SetStatFont(132);
#// Histogram titles
ROOT.gStyle.SetTitleFont(132, 'h');
ROOT.gStyle.SetTitleSize(0.06, 'h');
ROOT.gStyle.SetTitleBorderSize(0);
#// Axis titles
ROOT.gStyle.SetTitleFont(132, 'xyz');
ROOT.gStyle.SetTitleSize(0.04, 'xyz'); #0.05
ROOT.gStyle.SetTitleOffset(1.25, 'x');
ROOT.gStyle.SetTitleOffset(1., 'y');
#// Axis labels
ROOT.gStyle.SetLabelFont(132, 'xyz');
ROOT.gStyle.SetLabelSize(0.04, 'xyz'); #0.025
#// Text options
ROOT.gStyle.SetTitleFont(132, 't');
ROOT.gStyle.SetTextFont(132);
ROOT.gStyle.SetPaintTextFormat("3.2g");
ROOT.gStyle.SetTextSize(0.04)
ROOT.gROOT.ForceStyle();




'''
MindYourDecisions

A single alien lands on Earth. Every day after that, each alien on Earth undergoes a transformation, which could be any of the four equally likely events: (a) the alien dies, (b) the alien does nothing, (c) the alien replicates itself (2 aliens total), (d) the alien replicates itself twice (3 aliens total). With bad luck the alien race might die out quickly. But with good luck the alien race might survive indefinitely. What is the probability the alien race eventually dies out and goes extinct? This video presents a solution to this problem, which has been asked as a technical job interview question.
'''


### It appears that if the aliens live and replicate for long enough,
# their survival is guaranteed. But how many days does it take for a population
# to be guaranteed to no go extinct? Let's find out...


AliensEnd = []      # Number of aliens at end of iteration
AliensEndDays = []  # Number of days aliens survived for (max 50, below)
ExtinctM = []       # Log of survival rate. Essentially logging the Law of
                    # Large Numbers. Note: Returing to this code, not sure what
                    # the "M" is supposed to be...

# How many times do we land a single alien on Earth?
Iterations = 10000 #10000, 200000... 10000 is plenty.


print ''
print 'Iter.', '\t| Days','\t| Aliens', '|', 'Extinct %'
print '--------------------------------------------'
print '', 0, '\t| ', 0, '  \t| ', 1

for i in range(1, Iterations+1):
  # Should be able to optimize the number of days...
  # Maybe 50 is better and faster than 999.
  Days = 50 #50; 999
  Aliens = 1

  '''
  for d in range(1, Days+1):
    AliensCurrent = Aliens 
    
    for a in range(1, AliensCurrent+1):
      Event = np.random.randint(4)-1 # -1, +0, +1, +2
      AliensNew = AliensCurrent + Event
 
      if AliensNew == 0: 
        break
  
    Aliens = AliensNew
    if Aliens == 0:
      break
   '''

  # Over large iterations, letting whole population chose a single Event 
  # smooths out to equate to each alien chosing Event individually.
  for d in range(1, Days+1):
    Aliens = Aliens + np.random.randint(4)-1 # -1, +0, +1, +2
    # If aliends go extinct:
    if Aliens == 0:
      break

  AliensEnd.append(Aliens)
  AliensEndDays.append(d)
  ExtinctM.append(len([e for e in AliensEndDays if e < Days])/np.double(i))
  
  # Print every Iterations/10 iterations: ten updates.
  if (i % (Iterations/10)) == 0:
    print '', i, '\t| ', d, '\t| ', AliensEnd[-1], '\t | ', ExtinctM[-1]*100, '%'

print '--------------------------------------------'
print 'Iter.', '\t| Days','\t| Aliens', '|', 'Extinct %'



# Percent survived, extinct.
# If population made it to ~50 Days, consider nonextinctable.
# If population died before Days, consider (obviously) extinct.
Survived = len([s for s in AliensEndDays if s == Days])/np.double(Iterations)
Extinct = len([e for e in AliensEndDays if e < Days])/np.double(Iterations)

print ''
print ' Iterations   ', Iterations #i,'/',Iterations
print ' Max days     ', Days #d,'/',Days
print ' Survived     ', Survived*100., '%'
print ' Extinct      ', Extinct*100., '%'
print ''
# If only one alien did Event, then it seems to approach
#   Extinct       41.4 %
#   Survived      58.5 %
# But that was wrong? Now I loop over all aliens so they can all do Event.
# This current method results in
#   Extinct       41.6 %
#   Survived      58.4 %
# Maybe after enough iterations, the first IS correct...
# E.g. 1-1+1-1+1-1...=0 == 1+1+1+...-1-1-1...=0
# Reverted back to first method because I couldn't get the "correct" way to
# work probably.
# Note: Returing to this code, not sure why the above mentions a disagreement...




y = np.array(ExtinctM)
x = np.array(np.double(range(1,Iterations+1)))

c = ROOT.TCanvas('c', 'c', 1366, 720)
c.SetLogx()
g = ROOT.TGraph(len(x), x, y)
g.SetTitle('Probability of Alien Extinction Reaches '+str(round(Extinct*100,2))+'% for '+str('{:,}'.format(Iterations))+' Iterations;Iterations;Extinction probability')
#g.SetMarkerStyle(ROOT.kCircle)
#g.GetXaxis().SetLimits(-0.1, CountStop)
#g.GetYaxis().SetRangeUser(0, int(max(cps)*1.2))
g.Draw('al')

c.SaveAs('AlienExtinction_approach_'+str(Days)+'Days_'+str(Iterations)+'Iters.png')




# To optimize number of days, find good compromize between number of
# iterations and accuracy of extinction by that point... Maybe visually
# inspect... From below histogram, seems like 999 days is too much.
# 50 is much better.
# Just below: part of previous method to determine optimize days to observe.
#y2 = np.array(sorted(np.double(AliensEndDays[:-1])))
#x2 = np.array(np.double(range(1,Days+1)))

c2 = ROOT.TCanvas('c2', 'c2', 1366, 720)
#c2.SetLogx()
c2.SetLogy()
c2.SetGrid(1,1)
h = ROOT.TH1D('h', 'h', Days+1, 1, Days)
h.SetTitle('Probability of Extinction as a Function of Observation Time;Observation time (d);Probability of extinction (%)')
#h.GetXaxis().SetNdivisions(521)
h.SetStats(0)
for i in AliensEndDays:
  h.Fill(i)
h.Scale(1./Iterations *100.)
h.SetLineWidth(2)
h.SetLineColor(ROOT.kRed)
h.Draw('h text')  

c2.SaveAs('AlienExtinction_optimize_'+str(Days)+'Days_'+str(Iterations)+'Iters.png')




print ''
print 'Runtime', round((time.time() - start_time)/60.,2), 'minutes'
